# text2cron

<p align="center">
  <img width="380" height="300" src="https://github.com/-replaceme-project-org/text2cron/raw/master/assets/images/logo.png" alt="text2cron logo">
</p>

> Converts human readable text to Cron expression

![Release](https://img.shields.io/github/release/-replaceme-project-org/text2cron.svg) ![GitHub code size in bytes](https://img.shields.io/github/languages/code-size/-replaceme-project-org/text2cron.svg) [![Codacy Badge](https://api.codacy.com/project/badge/Grade/1215d67606784c51be6e00b5c277c8ea)](https://www.codacy.com/app/kolarski/text2cron?utm_source=github.com&utm_medium=referral&utm_content=-replaceme-project-org/text2cron&utm_campaign=Badge_Grade) <a href="https://codeclimate.com/github/-replaceme-project-org/text2cron/maintainability"><img src="https://api.codeclimate.com/v1/badges/9aa2c2c7ac55eea0c049/maintainability" /></a> <a href="https://codeclimate.com/github/-replaceme-project-org/text2cron/test_coverage"><img src="https://api.codeclimate.com/v1/badges/9aa2c2c7ac55eea0c049/test_coverage" /></a> ![Dependencies](https://img.shields.io/david/-replaceme-project-org/text2cron.svg) [![Build Status](https://travis-ci.org/-replaceme-project-org/text2cron.svg?branch=master)](https://travis-ci.org/-replaceme-project-org/text2cron)

## Usage

### Import the library

```html
<script src="https://cdn.jsdelivr.net/npm/text2cron/-replaceme-filename"></script>
```

or

```bash
yarn add text2cron
```

or

```
npm install text2cron --save
```

## Documentation

... Work in progress ...

## Contribution

Feel free to add suggestions, PRs, comments and bug reports.

## Authors

Alex Kolarski (aleks.rk@gmail.com)

## License

(The MIT License)

Copyright (c) 2019

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
'Software'), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
