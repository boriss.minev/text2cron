interface ICron {
  seconds: string;
  minutes: string;
  hours: string;
  dayOfMonth: string;
  month: string;
  dayOfWeek: string;
  year: string;
}
export class Text2Cron {
  public cronvert(text: string) {
    let cron: ICron = this.getDefault();

    // tslint:disable-next-line: max-line-length
    const regex = /.*(?<everyMatch>(every\s(?<everyTerm>second|minute|hour|day|week|month|year|monday|tuesday|wednesday|thursday|friday|saturday|sunday|january|february|march|april|may|june|july|august|september|october|november|december)))/gim;
    const res = regex.exec(text.toLowerCase());
    if (res && res.groups && res.groups.everyMatch) {
      console.log(res.groups.everyMatch);
      console.log(res.groups.everyTerm);
      switch (res.groups.everyTerm) {
        case "second":
          cron = {
            ...cron,
            seconds: "*",
            minutes: "*",
            hours: "*",
            dayOfMonth: "*",
            month: "*",
            dayOfWeek: "*",
            year: "*",
          };
          break;
        case "month":
          cron = {
            ...cron,
            month: "*",
            dayOfWeek: "*",
            year: "*",
          };
          break;
      }
    } else {
      throw new Error(`Can't convert "${text}" to cron`);
    }
    console.log(cron);
    if (!this.isValidCron(cron)) {
      throw new Error(`Can't convert "${text}" to cron`);
    }
    return this.cronToString(cron);
  }

  private cronToString(cron: ICron): string {
    return `${cron.seconds} ${cron.minutes} ${cron.hours} ${cron.dayOfMonth} ${cron.month} ${cron.dayOfWeek} ${cron.year}`;
  }

  private isValidCron(cron: ICron): boolean {
    return !Object.values(cron).includes(null);
  }

  private getDefault() {
    return {
      seconds: "0",
      minutes: "0",
      hours: "0",
      dayOfMonth: "1",
      month: "1",
      dayOfWeek: "1",
      year: "*",
    };
  }
}
