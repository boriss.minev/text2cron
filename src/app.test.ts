import { Text2Cron } from "./app";

const app = new Text2Cron();

test("lorem imsum text text asdfjql", () => {
  expect(() => {
    app.cronvert("lorem imsum text text asdfjql");
  }).toThrow(`Can't convert "lorem imsum text text asdfjql" to cron`);
});

test("execute this task every second", () => {
  expect(app.cronvert("execute this task every second")).toBe("* * * * * * *");
});
test("execute this task every Month", () => {
  expect(app.cronvert("execute this task every Month")).toBe("0 0 0 1 * * *");
});

// test("remind me to pay rent on 21st day of the month", () => {
//   expect(app.cronvert("remind me to pay rent on 21st day of the month")).toBe("* * * * * * *");
// });

// test('remind @peter tomorrow "Please review the office seating plan"', () => {
//   expect(app.cronvert('remind @peter tomorrow "Please review the office seating plan"')).toBe("* * * * * * *");
// });

// test("remind me to drink water at 3pm every day", () => {
//   expect(app.cronvert("me to drink water at 3pm every day")).toBe("0 0 15 ? * * *");
// });

// test("remind me on June 1st to wish Linda happy birthday", () => {
//   expect(app.cronvert("remind me on June 1st to wish Linda happy birthday")).toBe("* * * * * * *");
// });

// test("remind me to drink water at 3pm every day", () => {
//   expect(app.cronvert("me to drink water at 3pm every day")).toBe("* * * * * * *");
// });

// test('remind me to "Update the project status today" every Monday at 9am', () => {
//   expect(app.cronvert('remind me to "Update the project status today" every Monday at 9am')).toBe("* * * * * * *");
// });

// test("remind @jessica about the interview in 3 hours", () => {
//   expect(app.cronvert("remind @jessica about the interview in 3 hours")).toBe("* * * * * * *");
// });
// test('remind @peter tomorrow "Please review the office seating plan"', () => {
//   expect(app.cronvert('remind @peter tomorrow "Please review the office seating plan"')).toBe("* * * * * * *");
// });
